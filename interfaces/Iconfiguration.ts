export interface Gallery {
  photo_file: string;
}

export interface IConfiguration {
  male_name: string;
  male_nickname: string;
  male_photo: string;
  male_description: string;
  female_name: string;
  female_nickname: string;
  female_photo: string;
  female_description: string;
  akad_date: string;
  akad_name: string;
  akad_time: string;
  akad_longitude: string;
  akad_latitude: string;
  akad_maps: string;
  walimah_date: string;
  walimah_name: string;
  walimah_time: string;
  walimah_longitude: string;
  walimah_latitude: string;
  walimah_maps: string;
  qris_file: string;
  protocol_file: string;
  song_file: string;
  bank_account_number: string;
  bank_account_name: string;
  bank_account: string;
  zoom_live_url: string;
  meet_live_url: string;
  instagram_live_id: string;
  gallery: Gallery[];
}
