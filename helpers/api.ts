import axios, { AxiosResponse } from "axios";
import {
  getRefreshToken,
  getToken,
  setSession,
  setRefreshSession,
  setLogout,
  setCookie,
} from "./cookie";

interface IConfigAuth {
  username: string;
  password: string;
}

export interface IConfig {
  auth?: IConfigAuth;
  baseUrl: string;
  method: string;
  params?: any;
  uri: string;
}

interface IHeaders {
  Accept: string;
  Authorization: string;
}

export interface IFetchData {
  method: string;
  uri: string;
  params?: any;
  body?: any;
  callback?: any;
  additionalHeader?: any;
}

const getBaseUrl = (): string => {
  const apiUrl: string = `${process.env.REACT_API_URL}`;
  return apiUrl;
};

export const baseUrl: string = getBaseUrl();
const urlSelf: string = "";

export async function callAPIs<T>({
  method,
  uri,
  params,
  additionalHeader,
  baseUrl,
  auth = "",
}: any) {
  const baseUri: string = baseUrl || "";
  // let limitRequest:number = 0;
  const url: string = `${baseUri}/${uri}`;
  const headers: IHeaders = { ...additionalHeader };
  const dataOrParams = ["GET", "DELETE"].includes(method.toUpperCase())
    ? "params"
    : "data";
  const defaultConfig: object = { method, headers, url };
  const config = { ...defaultConfig, [dataOrParams]: params };

  if (auth) {
    Object.assign(config, { auth });
  }

  try {
    const response: AxiosResponse = await axios(config);
    return response;
  } catch (error: any) {
    if ([401].includes(error?.response?.status)) {
      return doRefreshToken({
        method,
        uri,
        params,
        additionalHeader,
        baseUrl,
        auth,
      });
    } else if ([400].includes(error?.response?.status)) {
		console.log(error.response.data.message);
	} else {
      setLogout();
    }
  }
}

export const refreshToken = async () => {
  const token: string = getRefreshToken() || "";
  const response: any = await axios({
    data: {
      refreshToken: token,
    },
    method: "POST",
    url: `${process.env.API_HOST}/api/v1/refresh`,
    auth: {
      username: process.env.API_BASIC_USERNAME || "",
      password: process.env.API_BASIC_PASS || "",
    },
  });

  setRefreshSession(response.data.data);
};

const doRefreshToken = async ({
  method,
  uri,
  params,
  additionalHeader,
  baseUrl,
  auth = "",
}: any) => {
  try {
    await refreshToken();

    const response: any = await callAPIs({
      method,
      uri,
      params,
      additionalHeader,
      baseUrl,
      auth,
    });
    return response;
  } catch (e) {
    console.error("Failed to refresh token", e);
    setLogout();
  }
};

export const generateToken = async () => {
  const response: any = await axios({
    data: {
      email: `${process.env.API_EMAIL}` || "",
      password: `${process.env.API_PASSWORD}` || "",
    },
    method: "post",
    headers: {
      "Content-Type": "application/json",
    },
    url: `${process.env.API_URL}/api/v1/login`,
    auth: {
      username: `${process.env.API_BASIC_USERNAME}` || "",
      password:
        `${"$2y$10$OrxvzHG9C5EqY7koQU9t6OKUtkKuFRPmrdubdTXRhzGc5mV5IEheG"}` ||
        "",
    },
  });

  return response.data.data;
};

export default callAPIs;
