import Cookies from "universal-cookie";
import Router from "next/router";

// const ls = require("local-storage");

/* tslint:disable:no-var-requires */
const CryptoJS = require("crypto-js");

const cookies = new Cookies();
const projectName = () => "weDd1n6__";
const projectEncrypt = () => "7mqtgr_HJN/CkX?6eZriz=@327$ftFXH";

export const cookieDomain = () => {
  const hostName = window.location.hostname;
  const hostNameArray = hostName.split(".");

  const cookieCrossDomain =
    hostNameArray.length > 1
      ? `.${hostNameArray
          .filter((val, index) => index >= hostNameArray.length - 2)
          .join(".")}` // get last 2 words from hostname if array length > 1 (e.g. telkomdigitalsolution.co) and append them with '.' (ASCII 46)
      : hostName; // get hostName if array length = 1, e.g. localhost

  return cookieCrossDomain;
};

export const setCookie = (cookieName: string, objCookies: any) => {
  const ciphertext = CryptoJS.AES.encrypt(
    JSON.stringify(objCookies),
    projectEncrypt()
  );
  // ls.set(cookieName, ciphertext.toString());
  // window.localStorage.setItem(cookieName, ciphertext.toString());
  cookies.set(cookieName, ciphertext.toString(), {
    path: "/",
    domain: cookieDomain(),
    httpOnly: false,
    secure: false,
  });
  return cookies;
};

export const setSession = (data: any) => {
  
  const authCookies = {
    [`${projectName()}_accessToken`]: data.access_token,
    // [`${projectName()}_refreshToken`]: data.refreshToken,
    // [`${projectName()}_accessTokenExpiresIn`]: data.accessTokenExpiresIn,
    // [`${projectName()}_refreshTokenExpiresIn`]: data.refreshTokenExpiresIn,
    // [`${projectName()}_subscribe`]: data.subscribe,

    // [`${projectName()}_photoProfile`]: data.photoProfile,
    [`${projectName()}_fullName`]: data.user.name,
    // [`${projectName()}_phoneNumber`]: data.phoneNumber,
    // [`${projectName()}_email`]: data.email,
    // [`${projectName()}_customerAccountName`]: data.customerAccountName,
  };

  setCookie("auth", authCookies);
};

export const setRefreshSession = ({
  accessToken,
  accessTokenExpiresIn,
}: any) => {
  const authCookies = {
    [`${projectName()}_accessToken`]: accessToken,
    // [`${projectName()}_refreshToken`]: getRefreshToken(),
    // [`${projectName()}_accessTokenExpiresIn`]: accessTokenExpiresIn,
    // [`${projectName()}_refreshTokenExpiresIn`]: getRefreshTokenExpired(),
    // [`${projectName()}_subscribe`]: getSubscribe(),
    // [`${projectName()}_photoProfile`]: getPhotoProfile(),
    [`${projectName()}_fullName`]: getFullName(),
    // [`${projectName()}_phoneNumber`]: getPhoneNumber(),
    [`${projectName()}_email`]: getEmail(),
    // [`${projectName()}_customerAccountName`]: getCustomerAccountName(),
  };

  clearCookie("auth");
  setTimeout(() => setCookie("auth", authCookies), 300);
};

export const getCookie = (cookieName: any, str: any) => {
  // if (window.localStorage.getItem(cookieName) !== undefined) {
  //   const bytes = CryptoJS.AES.decrypt(window.localStorage.getItem(cookieName), projectEncrypt());
  //   // const bytes = CryptoJS.AES.decrypt(cookies.get(cookieName), projectEncrypt());
  //   const decryptedData = JSON.parse(bytes.toString(CryptoJS.enc.Utf8));
  //   return decryptedData[str];
  // }
  if (null !== cookies.get(cookieName)) {
    const bytes = CryptoJS.AES.decrypt(
      cookies.get(cookieName),
      projectEncrypt()
    );
    const decryptedData = JSON.parse(bytes.toString(CryptoJS.enc.Utf8));
    return decryptedData[str];
  }
  return null;
};

export const getFullName = () => getCookie("auth", `${projectName()}_fullName`);
export const getEmail = () => getCookie("auth", `${projectName()}_email`);
export const getToken = () => getCookie("auth", `${projectName()}_accessToken`);
export const getRefreshToken = () =>
  getCookie("auth", `${projectName()}_refreshToken`);
// export const getTokenExpired = () =>
//   getCookie("auth", `${projectName()}_accessTokenExpiresIn`);
// export const getRefreshTokenExpired = () =>
//   getCookie("auth", `${projectName()}_refreshTokenExpiresIn`);
// export const getSubscribe = () =>
//   getCookie("auth", `${projectName()}_subscribe`);

export const clearCookie = (cookieName: any) => {
  cookies.remove(cookieName, {
    path: "/",
    domain: cookieDomain(),
    httpOnly: false,
    secure: false,
  });
};

export const setLogout = () => {
  clearCookie("auth");
  Router.reload();
};
