const path = require("path");
const webpack = require("webpack");
require("dotenv").config();

const nextConfig = {
  webpack(config, _options) {
    // Fixes npm packages that depend on `fs` module
    config.node = {
      fs: "empty",
    };

    // Returns environment variables as an object
    const env = Object.keys(process.env).reduce((acc, curr) => {
      acc[`process.env.${curr}`] = JSON.stringify(process.env[curr]);
      return acc;
    }, {});
    config.plugins.push(new webpack.DefinePlugin(env));

    return config;
  },
};

module.exports = {
  ...nextConfig,
  exportPathMap: async function () {
    const paths = {
      "/": { page: "/" },
    };
    return paths; //<--this was missing previously
  },
  webpack5: false,
  async headers() {
    return [
      {
        // matching all API routes
        source: "/api/:path*",
        headers: [
          { key: "Access-Control-Allow-Credentials", value: "true" },
          { key: "Access-Control-Allow-Origin", value: "*" },
          {
            key: "Access-Control-Allow-Methods",
            value: "GET,OPTIONS,PATCH,DELETE,POST,PUT",
          },
          {
            key: "Access-Control-Allow-Headers",
            value:
              "X-CSRF-Token, X-Requested-With, Accept, Accept-Version, Content-Length, Content-MD5, Content-Type, Date, X-Api-Version",
          },
        ],
      },
    ];
  },
};
