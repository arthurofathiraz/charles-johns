import { useRouter } from "next/router";
import { useState } from "react";
import { IConfig, callAPIs } from "../../../helpers/api";
import { getToken } from "../../../helpers/cookie";

export default function useHome() {
  const [configuration, setConfiguration] = useState({});

  const baseUrl = `${process.env.API_URL}`;
  
  const homeService = (params: any) => ({
    additionalHeader: {
      "Content-Type": "application/json",
      // Authorization: `Bearer ${getToken()}`,
    },
    baseUrl,
    method: "get",
    params,
    uri: `/v1/configuration`,
  });

  const fetchHome = async (params = {}) => {
    const payload: IConfig = homeService(params);

    try {
      const response: any = await callAPIs(payload);
      
    } catch (error) {
      console.log("err", error);
    }
  };

  return {
    configuration,
    fetchHome,
  };
}
