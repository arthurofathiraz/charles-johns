import {keyframes} from "@emotion/react";
import styled from "@emotion/styled";
import facepaint from "facepaint";
import {FC} from "react";
import Color from "../../assets/color";

const breakpoints = [1199.98, 991.98, 767.98, 479.98];
const mq = facepaint(breakpoints.map((bp) => `@media (max-width: ${bp}px)`));

const fadeIn = keyframes`
  from {
	opacity: 0;
	transform: translateY(50px);
  }

  to {
	opacity: 1;
	transform: translateY(0);
  }
`;

const ldsRipple = keyframes({
	"0%": {
		top: 36,
		left: 36,
		width: 0,
		height: 0,
		opacity: 1,
	},

	"100%": {
		top: 0,
		left: 0,
		width: 72,
		height: 72,
		opacity: 0,
	},
});

interface IAlert {
	fail?: boolean;
	success?: boolean;
}

export interface IProps {
	mobile?: boolean;
}

export const HomeStyled = styled.div(
	mq({
		minHeight: "100vh",
		width: "100%",

		".section": {
			backgroundSize: "cover",
			backgroundPosition: "center",
			minHeight: "100vh",
			maxHeight: "100vh",
			width: "100%",
			display: "flex",
			alignItems: "center",

			figure: {
				backgroundColor: "rgb(240,233,233, 0.6)",
				top: 0,
				left: 0,
				bottom: 0,
				right: 0,
				position: "absolute",
				zIndex: 0,
				margin: 0,
				padding: 0,
			},

			"&.section-one": {
				backgroundSize: ["70%", undefined, undefined, "100%"],
				backgroundRepeat: "no-repeat",
				"& > div": {
					display: "flex",
					flexWrap: "wrap",
					alignItems: "center",
					width: "100%",
					color: Color.light,
					position: "relative",
					zIndex: 10,
					"& > div, & > span": {
						width: "100%",
					},
					"& > div": {
						display: "flex",
						flexDirection: "column",
						alignItems: "center",
						padding: "0 40px",
						img: {
							maxWidth: 200,
							marginBottom: 40,
						},
						"& > strong": {
							fontSize: 34,
							fontWeight: 700,
							fontFamily: "'signatureFont'",
							textTransform: "capitalize",
							textAlign: "center",
							display: [undefined, undefined, undefined, "flex"],
							flexDirection: [undefined, undefined, undefined, "column"],
							lineHeight: 1,
							span: {
								fontSize: 27,
								color: Color.font.cream,
								marginTop: 10,
								marginBottom: 10,
							},
						},
						span: {
							fontSize: 18,
						},
						hr: {
							width: "30%",
							height: 2,
							backgroundColor: Color.font.cream,
							border: 0,
							margin: "15px 0 25px 0",
						},
					},
					"& > span": {
						fontSize: 15,
						textAlign: "center",
						fontFamily: "'signatureFont'",
						textTransform: "capitalize",
						color: Color.font.cream,
						marginBottom: 15,
					},
				},
				".to": {
					textAlign: "center",
					fontSize: 12,
					"& > strong": {
						fontSize: 18,
						"& + p": {
							color: Color.font.whiteGlow,
						},
					},
				},
				button: {
					marginTop: 24,
					backgroundColor: Color.font.cream,
					color: Color.light,
					width: 300,
					padding: 16,
					borderRadius: 50,
					border: 0,
					cursor: "pointer",
					transition: "all 500ms",
					"&:hover": {
						backgroundColor: Color.brownShowHover,
					},
				},
			},
			".sound": {},
		},
	})
);

export const SectionTwoStyled = styled.div(
	mq({
		display: ["grid", undefined, undefined, "flex"],
		gridTemplateColumns: "repeat(2, 1fr)",
		flexDirection: [undefined, undefined, undefined, "column"],
		overflowY: [undefined, undefined, undefined, "auto"],
		height: [undefined, undefined, undefined, "100vh"],
		position: "relative",

		"& > div": {
			display: "flex",
			alignItems: "center",
			justifyContent: "center",
			flexDirection: "column",
			backgroundColor: Color.brownCustom,
			// color: Color.light,

			"& > span": {
				fontFamily: "'signatureFont'",
				fontSize: [22, undefined, undefined, 18],
				color: Color.font.cream,
				textTransform: "lowercase",
				marginBottom: 15,
			},

			"& > p": {
				"> strong": {
					color: Color.light,
				},
			},

			"& > div": {
				"& > strong": {
					fontFamily: "'signatureFont'",
					fontSize: [42, undefined, undefined, 28],
					color: Color.light,
					"& > span": {
						fontSize: 28,
						color: Color.font.cream,
					},
				},
			},
			"&:first-of-type": {
				p: {
					marginBottom: [undefined, undefined, undefined, 0],
				},
				minHeight: [undefined, undefined, undefined, "100vh"],
				padding: [undefined, undefined, undefined, 24],
			},
			"& + div": {
				backgroundColor: Color.light,
				flexDirection: "column",
				maxWidth: "100%",
				height: [
					"calc(100vh - 160px)",
					"calc(100vh - 120px)",
					"calc(100vh - 80px)",
					"unset",
				],
				overflowY: ["auto", undefined, undefined, "unset"],
				marginTop: 0,
				justifyContent: "flex-start",
			},

			"& > img": {
				maxWidth: 500,
			},
		},
	})
);

export const ContentStyled = styled.div(
	mq({
		padding: [80, 60, 40],
		display: "flex",
		alignItems: "center",
		flexDirection: "column",
		textAlign: "center",
	})
);

export const FontSignatureStyled = styled.div({
	fontFamily: "'signatureFont'",
	textTransform: "capitalize",
	fontSize: 18,
	lineHeight: 1,
	marginTop: 80,
	marginBottom: 24,
	color: "#5a3a30",
	fontWeight: 900,
});

export const MempelaiStyled = styled.div(
	mq({
		width: "100%",
		display: "flex",
		alignItems: "center",
		flexDirection: "column",

		".wrapper-image": {
			//  border: "1px solid #dedede",
			borderRadius: 16,
			boxShadow: "rgb(0 0 0 / 75%) -5px 2px 29px -10px",

			"> img": {
				width: "100%",
				display: "block",
				borderRadius: 16,
			},
		},

		".google-maps": {
			display: "flex",
			flexDirection: "column",
			alignItems: "center",

			"> img": {
				width: "60%",
			},

			"> button": {
				marginTop: 24,
				backgroundColor: Color.brownCustom,
				color: Color.light,
				width: 300,
				padding: 16,
				borderRadius: 50,
				border: 0,
				cursor: "pointer",
				transition: "all 500ms",
				"&:hover": {
					backgroundColor: "darkblue",
				},
			},
		},

		".rekening": {
			display: "flex",
			flexDirection: "column",
			alignItems: "center",
			borderRadius: 3,
			backgroundColor: "#fafafa",
			marginBottom: 15,
			textAlign: "center",
			color: "#828282",
			fontSize: 14,
			fontWeight: 500,
			webkitBoxShadow: [0, 0, 5, 0, "rgba(0,0,0,.2)"],
			padding: 16,

			"> p": {
				margin: 0,
			},

			"> button": {
				"&.btn-copy": {
					marginTop: 10,
					//  backgroundColor: "rgb(107, 198, 16)",
					backgroundColor: Color.brownCustom,
					color: Color.light,
					width: "100%",
					padding: 10,
					borderRadius: 50,
					border: 0,
					cursor: "pointer",
					transition: "all 500ms",
					display: "flex",
					alignItems: "center",
					justifyContent: "center",
					"&:hover": {
						backgroundColor: "darkblue",
					},
					img: {
						width: 20,
						marginRight: 8,
					},
				},
			},
		},

		".streaming": {
			display: "flex",
			flexDirection: "column",
			alignItems: "center",

			"> img": {
				width: "60%",
			},

			"> button": {
				marginTop: 24,
				backgroundColor: Color.brownCustom,
				color: Color.light,
				width: 300,
				padding: 16,
				borderRadius: 50,
				border: 0,
				cursor: "pointer",
				transition: "all 500ms",
				"&:hover": {
					backgroundColor: "darkblue",
				},
			},
		},
		"& > .form-rsvp": {
			display: "flex",
			flexDirection: "column",
			width: "100%",
			"& > *": {
				marginTop: 16,
				padding: "8px 16px",
			},
			"& > input": {
				padding: "8px 16px",
			},
			"& > button": {
				padding: "12px",
				border: 0,
				backgroundColor: Color.brownCustom,
				borderRadius: 4,
				color: "white",
				cursor: "pointer",
				transition: "all 1s",
				fontSize: 16,
				"&:hover": {
					backgroundColor: Color.brownShowHover,
				},
			},
		},
	})
);

export const MempelaiOrangStyled = styled.div({
	display: "flex",
	alignItems: "center",
	width: "100%",
	position: "relative",
	"& > div": {
		display: "grid",
		gridTemplateColumns: "1fr 1fr",
		gap: 40,
		width: "100%",
	},
});

export const InfoMempelaiStyled = styled.div(
	mq({
		display: "flex",
		flexDirection: "column",
		alignItems: "center",
		textAlign: "center",
		"& > h3": {
			marginTop: 0,
			fontWeight: 800,
		},
		"& > span": {
			color: Color.font.pencil,
		},
		"& > div": {
			position: "relative",
			width: [240, 180, 160, 140],
			height: [240, 180, 160, 140],
			boxShadow: "rgb(0 0 0 / 75%) -5px 2px 29px -10px",
			margin: "25px auto",
			// borderRadius: 6,
			backgroundSize: "cover",
			backgroundRepeat: "no-repeat",
			backgroundPositionX: "center",
			backgroundPositionY: "top",
			borderRadius: "100% 100% 32px 100%",
		},
	})
);

export const GridGaleriStyled = styled.div(
	mq({
		display: "grid",
		gridTemplateColumns: "repeat(2, 1fr)",
		gap: 40,
		width: "100%",
		marginBottom: 40,

		"& > div": {
			position: "relative",
			height: [300, undefined, undefined, 200],
			boxShadow: "rgb(0 0 0 / 75%) -5px 2px 29px -10px",
			margin: ["25px auto", undefined, undefined, "8px auto"],
			width: "100%",
			backgroundSize: "cover",
			backgroundRepeat: "no-repeat",
			backgroundPositionX: "center",
			backgroundPositionY: "top",
			border: `4px solid ${Color.light}`,
		},
	})
);

export const AndStyled = styled.div(
	mq({
		position: "absolute",
		left: "50%",
		top: ["40%", undefined, undefined, "30%"],
		transform: "translate(-50%,-50%)",
		width: 60,
		height: 60,
		backgroundColor: Color.brownCustom,
		display: "flex",
		alignItems: "center",
		justifyContent: "center",
		zIndex: 1,
		borderRadius: "50%",
		color: Color.light,
		fontSize: 32,
	})
);

export const WrapperCounter = styled.div({
	display: "flex",
	alignItems: "center",
	margin: "16px 0px",

	"> div": {
		display: "flex",
		flexDirection: "column",
		marginRight: 16,
	},
});

export const RsvpWrapperList = styled.div({});

export const AlertStyled = styled.div<IAlert>(({fail}) => ({
	backgroundColor: fail ? Color.danger.default : Color.approval.default,
	color: Color.light,
	fontSize: 14,
	display: "block",
	width: "100%",
	padding: "8px 16px",
	borderRadius: 2,
	boxSizing: "border-box",
	animation: `${fadeIn} 1s ease`,
}));

export const LdsRippleStyled = styled.div({
	display: "inline-block",
	position: "relative",
	width: 80,
	height: 80,
	div: {
		position: "absolute",
		border: "4px solid #1B3062",
		opacity: 1,
		borderRadius: "50%",
		animation: `${ldsRipple} 1s cubic-bezier(0, 0.2, 0.8, 1) infinite`,
		"&:nth-of-type(2)": {
			animationDelay: "-0.5s",
		},
	},
});

export const DateAkadStyled = styled.div(
	mq({
		marginTop: 40,
		"& > div": {
			"& > div": {
				"& > div": {
					marginTop: 24,
					marginBottom: 8,
					"& > div": {
						maxWidth: [undefined, undefined, undefined, 120],
					},
				},
				"& > p": {
					margin: 0,
				},
			},

			"& + p": {
				marginTop: 40,
			},
		},
	})
);

export const DressCodeStyled = styled.div(
	mq({
		"& > div": {
			ul: {
				display: "grid",
				gridTemplateColumns: "repeat(2, 1fr)",
				gap: 60,
				li: {
					img: {
						width: [100, undefined, undefined, 80],
					},
				},
			},
		},
	})
);

export const DatedStyled = styled.span(
	mq({
		display: "flex",
		alignItems: "center",
		justifyContent: "center",
		"& > strong": {
			fontSize: 60,
			fontWeight: 900,
			color: Color.brownCustom,
		},
		"& > span": {
			display: "flex",
			flexWrap: "wrap",
			alignContent: "space-between",
			height: 50,
			marginLeft: 16,
			width: 108,
			"& > p": {
				margin: 0,
				width: "100%",
				textAlign: "left",
				fontSize: 20,
			},
		},
	})
);

export const PlayMusicButtonStyled = styled.div(
	mq({
		position: "fixed",
		bottom: 25,
		right: 25,
		borderRadius: "50%",
		backgroundColor: Color.brownCustom,
		width: 40,
		height: 40,
		display: "flex",
		alignItems: "center",
		justifyContent: "center",
		cursor: "pointer",
		border: `2px solid ${Color.light}`,
		transition: "all 1s",
		img: {
			width: 25,
			transition: "all 1s",
		},
		"&:hover": {
			backgroundColor: Color.brownShowHover,
			img: {
				width: 32,
			},
		},
	})
);
