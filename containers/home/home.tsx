/* eslint-disable @next/next/no-img-element */
import React, {useEffect, useState, useRef} from "react";
import {HomeStyled, PlayMusicButtonStyled} from "./home.styles";
import ReactPageScroller from "react-page-scroller";
import {SectionOne, SectionTwo} from "./partials";
import {setSession} from "../../helpers/cookie";

export interface IHome {
	auth: any;
	configuration: any;
}

const HomeContainer: React.FC<IHome> = ({auth, configuration}: any) => {
	const [currentPage, setCurrentPage] = React.useState(0);
	const [playing, setPlaying] = useState(true);
	const toggle = () => {
		setPlaying(playing == true ? false : true);
	}

	const isBlockScrollUp = currentPage === 1;
	const isBlockScrollDown = currentPage === 0;

	// set audio player
	const audioPlayers = useRef<HTMLAudioElement | undefined>(
		typeof Audio !== "undefined" ? new Audio(configuration.song_file) : undefined
	);

	// set endless loop song
	audioPlayers.current?.addEventListener('ended', function () {
		this.currentTime = 0;
		this.play();
		setPlaying(true);
	}, false);

	// set play
	const isPlay = currentPage > 0 && playing == true;
	isPlay ? audioPlayers.current?.play() : audioPlayers.current?.pause();

	useEffect(() => {
		setSession(auth.response);
	}, [auth, playing]);

	return (
		<HomeStyled>
			<ReactPageScroller
				pageOnChange={(page: number) => setCurrentPage(page)}
				customPageNumber={currentPage}
				blockScrollUp={isBlockScrollUp}
				blockScrollDown={isBlockScrollDown}
			>
				<SectionOne setCurrentPage={setCurrentPage} configuration={configuration}/>
				<SectionTwo configuration={configuration} currentPage={currentPage}/>
			</ReactPageScroller>
			{
				currentPage > 0 && <PlayMusicButtonStyled onClick={toggle}>
					{
						playing == true && <img
						 src="https://res.cloudinary.com/caturteguh/image/upload/v1633564204/play-music-2_oinl9h.png"
						 alt=""
						/>
					}
					{
						playing == false && <img
						 src="https://res.cloudinary.com/dbdp4fear/image/upload/v1633570334/oie_8VvvzeOdWtJY_o3mjox.png"
						 alt=""
						/>
					}
				</PlayMusicButtonStyled>
			}
		</HomeStyled>
	);
};

export default HomeContainer;
