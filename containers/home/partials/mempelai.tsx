import React from "react";
import Fade from 'react-reveal/Fade';
import {
	AndStyled,
	ContentStyled,
	FontSignatureStyled,
	GridGaleriStyled,
	InfoMempelaiStyled,
	MempelaiOrangStyled,
	MempelaiStyled,
	SectionTwoStyled,
	WrapperCounter,
} from "../home.styles";
import labelData from "../../../config/label.json";
import {IConfiguration} from "../../../interfaces/Iconfiguration";

interface IMempelai {
	configuration: IConfiguration;
}

export const Mempelai: React.FC<IMempelai> = ({configuration}) => {
	return (
		<MempelaiStyled>
			<Fade duration={4000}>
				<FontSignatureStyled>{labelData.mempelai}</FontSignatureStyled>
			</Fade>
			<Fade duration={4000}>
				<MempelaiOrangStyled>
					<div>
						<InfoMempelaiStyled>
							<div
								style={{
									backgroundImage: `url(${configuration.male_photo})`,
								}}
							/>
							<h3>{configuration.male_name}</h3>
							<span>{configuration.male_description}</span>
						</InfoMempelaiStyled>
						<AndStyled>{"&"}</AndStyled>
						<InfoMempelaiStyled>
							<div
								style={{
									backgroundImage: `url(${configuration.female_photo})`,
								}}
							/>
							<h3>{configuration.female_name}</h3>
							<span>{configuration.female_description}</span>
						</InfoMempelaiStyled>
					</div>
				</MempelaiOrangStyled>
			</Fade>
		</MempelaiStyled>
	);
};

export default Mempelai;
