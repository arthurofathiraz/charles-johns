import React, {useRef, useEffect, useState} from "react";
import Countdown from "react-countdown";
import moment from "moment-timezone";
import Fade from "react-reveal/Fade";
import {
	ContentStyled,
	FontSignatureStyled,
	MempelaiStyled,
	SectionTwoStyled,
	WrapperCounter,
} from "../home.styles";
import contentData from "../../../config/content.json";
import labelData from "../../../config/label.json";
import {IConfiguration} from "../../../interfaces/Iconfiguration";
import Mempelai from "./mempelai";
import Kisah from "./kisah";
import Galery from "./galery";
import DateAkad from "./dateAkad";
import MapsAndStreaming from "./mapsAndStreaming";
import GiftRsvp from "./giftAndRsvp";
import DresscodeAndPanduan from "./dresscodeAndPanduan";

interface ISectionTwoPartial {
	currentPage: number;
	configuration: IConfiguration;
}

const SectionTwoPartial: React.FC<ISectionTwoPartial> = ({configuration, currentPage}) => {
	const renderer = ({days, hours, minutes, seconds, completed}) => {
		if (completed) {
			return <Completionist/>;
		} else {
			return (
				<WrapperCounter>
					<div>
						<span>{days}</span>
						<span>Days</span>
					</div>
					<div>
						<span>{hours}</span>
						<span>Hours</span>
					</div>
					<div>
						<span>{minutes}</span>
						<span>Minutes</span>
					</div>
					<div>
						<span>{seconds}</span>
						<span>Seconds</span>
					</div>
				</WrapperCounter>
			);
		}
	};

	const countDate = moment(configuration.walimah_date, "YYYY-MM-DD").tz(
		"Asia/Jakarta"
	);
	const Completionist = () => (
		<p>
			<span>The wait is over.</span>
		</p>
	);

	return (
		<SectionTwoStyled>
			<div>
				<Fade duration={4000}>
					<span>{labelData.walimah}</span>
				</Fade>
				<Fade duration={4000}>
					<div>
						<strong>
							{configuration.male_nickname} <span>&</span>{" "}
							{configuration.female_nickname}
						</strong>
					</div>
				</Fade>
				<Fade duration={4000}>
					{/* eslint-disable @next/next/no-img-element */}
					<img loading="lazy" src={`${contentData.background}`} alt=""/>
				</Fade>
				<Fade duration={4000}>
					<p>
						<strong>
							{moment(configuration.walimah_date).format("DD MMMM YYYY")}
						</strong>
					</p>
				</Fade>
			</div>
			<ContentStyled>
				<Fade duration={4000}>
					<FontSignatureStyled style={{marginTop: 0}}>
						{labelData.assalamuAlaikum}
					</FontSignatureStyled>
				</Fade>
				<Fade duration={4000}>
					<p>{labelData.ayat.text}</p>
					<p>{labelData.ayat.surah}</p>
				</Fade>
				<Fade duration={4000}>
					<FontSignatureStyled>{labelData.bismillah}</FontSignatureStyled>
				</Fade>
				<Fade>
					<p>{labelData.kalimatAwal}</p>
				</Fade>
				<Mempelai configuration={configuration}/>
				<DateAkad configuration={configuration}/>
				<MapsAndStreaming configuration={configuration}/>
				<DresscodeAndPanduan configuration={configuration}/>
				<Galery configuration={configuration}/>
				<Kisah configuration={configuration}/>
				<GiftRsvp configuration={configuration}/>
				<MempelaiStyled>
					<FontSignatureStyled></FontSignatureStyled>
					<Fade duration={4000}>
						{/* eslint-disable @next/next/no-img-element */}
						<img
							src="https://res.cloudinary.com/dbdp4fear/image/upload/v1641988435/save_the_date_hijab_1_esroi2.svg"
							alt=""
							style={{marginBottom: 10}}
						/>
					</Fade>
					<Fade duration={4000}>
						<Countdown date={countDate.toDate()} renderer={renderer}>
							<Completionist/>
						</Countdown>
					</Fade>
				</MempelaiStyled>
				<Fade duration={4000}>
					<FontSignatureStyled style={{marginTop: 100, marginBottom: 0}}>
						{labelData.wassalaamuAlaikum}
					</FontSignatureStyled>
				</Fade>
				<Fade duration={4000}>
					<p style={{fontSize: 12}}>{contentData.footer}</p>
				</Fade>
			</ContentStyled>
		</SectionTwoStyled>
	);
};

export default SectionTwoPartial;
