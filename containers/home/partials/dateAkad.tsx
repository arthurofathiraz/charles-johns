import React from "react";
import Fade from "react-reveal/Fade";
import moment from "moment";
import {
	DateAkadStyled,
	DatedStyled,
	FontSignatureStyled,
	MempelaiStyled,
} from "../home.styles";
import labelData from "../../../config/label.json";
import contentData from "../../../config/content.json";
import {IConfiguration} from "../../../interfaces/Iconfiguration";
import {useRouter} from "next/router";

interface IDateAkad {
	configuration: IConfiguration;
}

export const DateAkad: React.FC<IDateAkad> = ({configuration}) => {
	const router = useRouter();
	const {s} = router.query;

	return (
		<DateAkadStyled>
			<Fade duration={4000}>
				<p>Sunday / Minggu / Ahad</p>
				<DatedStyled>
					<strong>6</strong>
					<span>
					  <p>Maret</p>
					  <p>2022</p>
					</span>
				</DatedStyled>
				<MempelaiStyled>
					<FontSignatureStyled>{labelData.akadNIkah}</FontSignatureStyled>
					<p>{`${configuration.akad_time} WIB`}</p>
				</MempelaiStyled>
				<p style={{fontSize: 20, marginBottom: 5}}>{configuration.walimah_name}</p>
				<p style={{marginTop: 0}}>{configuration.akad_name}</p>
			</Fade>
		</DateAkadStyled>
	);
};

export default DateAkad;
