/* eslint-disable @next/next/no-img-element */
import React, { useEffect, useState } from "react";
import Fade from "react-reveal/Fade";
import ClipboardJS from "clipboard";
import {
	AlertStyled,
	FontSignatureStyled,
	LdsRippleStyled,
	MempelaiStyled,
} from "../home.styles";
import labelData from "../../../config/label.json";
import { IConfiguration } from "../../../interfaces/Iconfiguration";
import fetchRsvp from "../../../services/rsvp";
import { useRouter } from "next/router";

interface IGiftRsvp {
	configuration: IConfiguration;
}

export const GiftRsvp: React.FC<IGiftRsvp> = ({ configuration }) => {
	const [nama, setNama] = useState("");
	const [alamat, setAlamat] = useState("");
	const [hadir, setHadir] = useState("");
	const [isErrorValidate, setIsErrorValidate] = useState(false);
	const [isSuccessValidate, setIsSuccessValidate] = useState(false);
	const [isFailValidate, setIsFailValidate] = useState(false);
	const [isSubmitForm, setIsSubmitForm] = useState(false);
	const [isSuccessCopy, setIsSuccessCopy] = useState(false);
	const [isFailCopy, setIsFailCopy] = useState(false);
	const [isShowRsvp, setIsShowRsvp] = useState(true);

	// set rsvp
	const router = useRouter();
	const { rsvp } = router.query;

	const handleSubmit = async () => {
		const payload = {
			nama: nama,
			alamat: alamat,
			hadir: hadir,
		};

		// set submit loading
		handleShowSubmitLoading();

		const validation = [nama === "", alamat == "", hadir === ""];

		console.log("\n\n");
		console.log("wkwk : ", validation.includes(true));
		console.log("\n\n");

		if (validation.includes(true)) {
			handleShowModalAlert();
		} else {
			handleCloseModalAlert();

			const resultFetchRsvp = await fetchRsvp(payload);
			if (resultFetchRsvp && resultFetchRsvp.status === 200) {
				/*clear state */
				setNama("");
				setAlamat("");
				setHadir("");
				//sukses nanti render sukses
				setIsSuccessValidate(true);

				// remove label success after 5 second
				window.setTimeout(function () {
					setIsSuccessValidate(false);
				}, 5000);
			} else {
				setIsFailValidate(true);

				// remove label error after 5 second
				window.setTimeout(function () {
					setIsFailValidate(false);
				}, 5000);
			}

			// close submit loading
			handleCloseSubmitLoading();
		}
	};

	/* nanti ini dipake buat close modal error */
	const handleCloseModalAlert = () => {
		setIsErrorValidate(false);
	};
	const handleShowModalAlert = () => {
		setIsErrorValidate(true);
		handleCloseSubmitLoading();

		// remove label error after 5 second
		window.setTimeout(function () {
			setIsErrorValidate(false);
		}, 5000);
	};
	const handleShowSubmitLoading = () => {
		setIsSubmitForm(true);
		setIsSuccessValidate(false);
		setIsFailValidate(false);
	};
	const handleCloseSubmitLoading = () => {
		setIsSubmitForm(false);
	};

	useEffect(() => {
		const clipboard = new ClipboardJS(".btn-copy");
		clipboard.on("success", function (e) {
			e.clearSelection();

			// show label success
			setIsSuccessCopy(true);

			// remove label success after 3 second
			window.setTimeout(function () {
				setIsSuccessCopy(false);
			}, 3000);
		});

		clipboard.on("error", function (e) {
			// show label error
			setIsFailCopy(true);

			// remove label error after 3 second
			window.setTimeout(function () {
				setIsFailCopy(false);
			}, 3000);
		});

		setIsShowRsvp((rsvp && rsvp == "false") ? false : true);
	}, []);

	return (
		<>
			<MempelaiStyled>
				<Fade duration={4000}>
					<FontSignatureStyled>{labelData.gift}</FontSignatureStyled>
				</Fade>
				<Fade duration={4000}>
					<p>{labelData.giftDescription}</p>
				</Fade>
				<Fade duration={4000}>
					<div className="rekening" style={{ marginBottom: 30 }}>
						<p>
							<strong>{configuration.bank_account}</strong>
						</p>
						<p>
							No. Rekening: <strong>{configuration.bank_account_number}</strong>
						</p>
						<p>
							a.n: <strong>{configuration.bank_account_name}</strong>
						</p>
						<button
							className="btn-copy"
							data-clipboard-text={configuration.bank_account_number}
						>
							<img
								src="https://res.cloudinary.com/caturteguh/image/upload/v1633527169/icon-copy_jccrzm.png"
								alt=""
							/>
							{labelData.buttonRekening}
						</button>
						{isSuccessCopy && (
							<AlertStyled style={{ marginTop: 8 }}>Berhasil dicopy</AlertStyled>
						)}
						{isFailCopy && (
							<AlertStyled fail style={{ marginTop: 8 }}>
								Tidak berhasil dicopy, mohon copy secara manual
							</AlertStyled>
						)}
					</div>
				</Fade>
			</MempelaiStyled>
			<MempelaiStyled>
				<Fade duration={4000}>
					<FontSignatureStyled>{labelData.protocol}</FontSignatureStyled>
				</Fade>
				<Fade duration={4000}>
					<p>{labelData.protocolDescription}</p>
				</Fade>
				<Fade duration={4000}>
					<div className="wrapper-image">
						{/* eslint-disable @next/next/no-img-element */}
						<img loading="lazy" src={`${configuration.protocol_file}`} alt="" />
					</div>
				</Fade>
			</MempelaiStyled>
			<MempelaiStyled>
				{isShowRsvp && <Fade duration={4000}>
					<FontSignatureStyled>{labelData.bukutamu}</FontSignatureStyled>
				</Fade>}
				{isShowRsvp && <Fade duration={4000}>
					<p>{labelData.bukutamuDescription}</p>
				</Fade>}
				{isErrorValidate && <AlertStyled fail>Anda belum isi form</AlertStyled>}
				{isSuccessValidate && (
					<AlertStyled>
						Anda berhasil mengirimkan RSVP data
					</AlertStyled>
				)}
				{isFailValidate && (
					<AlertStyled fail>
						Anda gagal mengirimkan RSVP data, harap dicoba kembali
					</AlertStyled>
				)}
				{isSubmitForm && (
					<LdsRippleStyled>
						<div />
						<div />
					</LdsRippleStyled>
				)}
				{isShowRsvp && <div className="form-rsvp">
					<Fade duration={4000}>
						<input
							placeholder="Nama lengkap"
							required
							value={nama}
							onChange={(e: any) => setNama(e.target.value)}
							disabled={isSubmitForm}
						/>
					</Fade>
					<Fade duration={4000}>
						<input
							placeholder="Alamat"
							required
							value={alamat}
							onChange={(e: any) => setAlamat(e.target.value)}
							disabled={isSubmitForm}
						/>
					</Fade>
					<Fade duration={4000}>
						<select
							placeholder="Apakah anda akan hadir?"
							required
							value={hadir}
							disabled={isSubmitForm}
							onChange={(e: any) => setHadir(e.target.value)}
						>
							<option value="" disabled>Apakah anda akan hadir?</option>
							<option value="ya">Ya</option>
							<option value="tidak">Tidak</option>
						</select>
					</Fade>
					<Fade duration={4000}>
						<button type="submit" onClick={handleSubmit} disabled={isSubmitForm}>
							{labelData.buttonRsvp}
						</button>
					</Fade>
				</div>}
			</MempelaiStyled>
		</>
	);
};

export default GiftRsvp;
