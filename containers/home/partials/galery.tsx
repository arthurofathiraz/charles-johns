import React, {useState} from "react";
import Fade from "react-reveal/Fade";
import Lightbox from 'react-image-lightbox';
import 'react-image-lightbox/style.css';
import {
	FontSignatureStyled,
	GridGaleriStyled,
	MempelaiStyled,
} from "../home.styles";
import labelData from "../../../config/label.json";
import {IConfiguration} from "../../../interfaces/Iconfiguration";

interface IGalery {
	configuration: IConfiguration;
}

export const Galery: React.FC<IGalery> = ({configuration}) => {
	const [isShowGallery, setIsShowGallery] = useState(false);
	const [photoIndex, setPhotoIndex] = useState(0);

	// set gallery images to show
	let images: string[] = [];
	configuration.gallery.forEach(function (image) {
		images.push(image.photo_file);
	});

	const handleGallery = async (index) => {
		setIsShowGallery(true);
		setPhotoIndex(index);
	}

	return (
		<MempelaiStyled>
			<FontSignatureStyled>{labelData.galeri}</FontSignatureStyled>
			<GridGaleriStyled>
				{configuration.gallery.map((item, index) => (
					// eslint-disable-next-line react/jsx-key
					<Fade duration={4000}>
						<div
							key={index}
							style={{
								backgroundImage: `url(${item.photo_file})`,
								cursor: "pointer",
							}}
							onClick={() => handleGallery(index)}
						>
						</div>
					</Fade>
				))}
			</GridGaleriStyled>

			{isShowGallery && (
				<Lightbox
					mainSrc={images[photoIndex]}
					nextSrc={images[(photoIndex + 1) % images.length]}
					prevSrc={images[(photoIndex + images.length - 1) % images.length]}
					onCloseRequest={() => setIsShowGallery(false)}
					onMovePrevRequest={() =>
						setPhotoIndex((photoIndex + images.length - 1) % images.length)
					}
					onMoveNextRequest={() =>
						setPhotoIndex((photoIndex + 1) % images.length)
					}
					enableZoom={false}
				/>
			)}
		</MempelaiStyled>
	);
};

export default Galery;
