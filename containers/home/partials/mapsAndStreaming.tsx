import React from "react";
import Fade from "react-reveal/Fade";
import {FontSignatureStyled, MempelaiStyled} from "../home.styles";
import labelData from "../../../config/label.json";
import contentData from "../../../config/content.json";
import {IConfiguration} from "../../../interfaces/Iconfiguration";
import fetchBitly from "../../../services/bitly";

interface IMapsAndStreaming {
	configuration: IConfiguration;
}

export const MapsAndStreaming: React.FC<IMapsAndStreaming> = ({configuration}) => {

	let walimah_maps = configuration.walimah_maps;
	let meet_live_url = configuration.meet_live_url;

	// handle bitly
	const handleBitly = async () => {
		const resultBitlyMaps = await fetchBitly({
			long_url: configuration.walimah_maps,
		});
		if (resultBitlyMaps && resultBitlyMaps.data) {
			walimah_maps = resultBitlyMaps.data.link;
		}

		const resultBitlyMeet = await fetchBitly({
			long_url: configuration.meet_live_url,
		});
		if (resultBitlyMeet && resultBitlyMeet.data) {
			meet_live_url = resultBitlyMeet.data.link;
		}
	}

	// generate link for maps for analytics
	handleBitly();

	return (
		<>
			<MempelaiStyled>
				<Fade duration={4000}>
					<FontSignatureStyled>{labelData.googleMap}</FontSignatureStyled>
				</Fade>
				<Fade duration={4000}>
					{/* eslint-disable @next/next/no-img-element */}
					<img loading="lazy" src={`${labelData.googleMaps}`} alt=""/>
				</Fade>
				<Fade duration={4000}>
					<div className="google-maps">
						<button onClick={async () => {
							window.open(walimah_maps, "_blank");
						}}>
							{labelData.klikGoogleMaps}
						</button>
					</div>
				</Fade>
			</MempelaiStyled>
			<MempelaiStyled>
				<Fade duration={4000}>
					<FontSignatureStyled>{labelData.liveStreaming}</FontSignatureStyled>
				</Fade>
				<div className="streaming">
					<Fade duration={4000}>
						{/* eslint-disable @next/next/no-img-element */}
						<img loading="lazy" src={`${contentData.youtubeLogo}`} alt=""/>
					</Fade>
					<Fade duration={4000}>
						<button onClick={() => window.open(meet_live_url, "_blank")}>
							{labelData.klikDisini}
						</button>
					</Fade>
				</div>
			</MempelaiStyled>
		</>
	);
};

export default MapsAndStreaming;
