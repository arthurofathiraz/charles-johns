import React from "react";
import {useRouter} from "next/router";
import Fade from 'react-reveal/Fade';
import contentStatic from "../../../config/content.json";
import labelData from "../../../config/label.json";
import {IConfiguration} from "../../../interfaces/Iconfiguration";

interface ISectionOnePartial {
	setCurrentPage: (page: number) => void;
	configuration: IConfiguration;
}

const SectionOnePartial: React.FC<ISectionOnePartial> = ({setCurrentPage, configuration}) => {
	const {male_nickname, female_nickname} = configuration;
	const {buttonOpen, weddingInvitations, toMrMrs, title} = labelData;
	const router = useRouter();
	const {to} = router.query;

	return (
		<div className="section section-one" style={{backgroundImage: `url(${contentStatic.backgroundHome})`}}>
			<figure/>
			<div>
				<Fade duration={4000}>
					<span>{weddingInvitations}</span>
				</Fade>
				<div>
					<Fade duration={4000}>
						<strong className="signature">
							{male_nickname} <span>&</span> {female_nickname}
						</strong>
					</Fade>
					<Fade duration={4000}>
						<hr/>
					</Fade>
					<div className="to">
						<Fade duration={4000}>
							<p>{toMrMrs}</p>
						</Fade>
						<Fade duration={4000}>
							<strong>{to}</strong>
						</Fade>
						<Fade duration={4000}>
							<p>{title}</p>
						</Fade>
					</div>
					<Fade duration={4000}>
						<button onClick={() => setCurrentPage(1)}>{buttonOpen}</button>
					</Fade>
				</div>
			</div>
		</div>
	);
};

export default SectionOnePartial;
