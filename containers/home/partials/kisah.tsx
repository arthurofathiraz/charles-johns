import React, {useState} from "react";
import Fade from "react-reveal/Fade";
import Lightbox from 'react-image-lightbox';
import 'react-image-lightbox/style.css';
import {
	FontSignatureStyled,
	GridGaleriStyled,
	MempelaiStyled,
} from "../home.styles";
import labelData from "../../../config/label.json";
import {IConfiguration} from "../../../interfaces/Iconfiguration";

interface IKisah {
	configuration: IConfiguration;
}

export const Kisah: React.FC<IKisah> = ({configuration}) => {

	return (
		<MempelaiStyled>
			<Fade duration={4000}>
				<FontSignatureStyled style={{marginTop: 0}}>
					{labelData.kisah}
				</FontSignatureStyled>
			</Fade>
			<Fade duration={4000}>
				<p style={{marginBottom: 0}}><strong>Januari 2021</strong></p>
				<p style={{marginTop: 5}}>Kami berasal dari Kampus yang sama, salah satu Kampus yang ada di Jatinangor.
					Namun, kisah kami
					bermula dari dunia maya. Awal perkenalan melalui salah satu aplikasi jejaring sosial media, sejak
					saat itu Kami memutuskan untuk berkomunikasi lebih jauh melalui Whatsapp.</p>

				<p style={{marginBottom: 0}}><strong>Februari 2021</strong></p>
				<p style={{marginTop: 5}}>Kami lost contact, namun takdir Allah mempertemukan kembali. Komunikasi kami
					terjalin lagi, dan Kami memutuskan untuk berkenalan lebih dalam. </p>

				<p style={{marginBottom: 0}}><strong>Maret 2021</strong></p>
				<p style={{marginTop: 5}}>Kami bertemu untuk pertama kali. Juga bertemu dengan keluarga masing-masing
					untuk pertama kali. Kami memutuskan untuk menjalin hubungan ke arah yang lebih serius. </p>

				<p style={{marginBottom: 0}}><strong>November 2021</strong></p>
				<p style={{marginTop: 5}}>Di bulan ini kami melangsungkan lamaran, niat kami untuk beribadah melalui
					pernikahan dimulai dari sini. Kami jatuh cinta bukan kepada orang yang sempurna, melainkan karena
					melihat jika Kita berdua bisa saling menyempurnakan. </p>

				<p style={{marginBottom: 0}}><strong>Maret 2022</strong></p>
				<p style={{marginTop: 5}}>Dan insyaAllah pada 6 Maret 2022 kami akan melangsungkan pernikahan. Mengikat
					janji suci untuk menjadi teman hidup selamanya.</p>
			</Fade>
		</MempelaiStyled>
	);
};

export default Kisah;
