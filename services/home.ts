import axios, { AxiosResponse } from "axios";

export const fetchHome = async (token: string) => {
  const response: any = await axios({
    data: {},
    method: "get",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${token}`,
    },
    url: `${process.env.API_URL}/api/v1/configuration`,
  });

  return response.data.data;
};

export default fetchHome;
