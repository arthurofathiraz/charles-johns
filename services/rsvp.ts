import { useEffect, useState, useMemo } from "react";
import { callAPIs, IConfig } from "../helpers/api";
import { getToken } from "../helpers/cookie";

const baseUrl = `${process.env.API_URL}`;

const rsvpService = (params: any) => ({
  additionalHeader: {
    "Content-Type": "application/json",
    Authorization: `Bearer ${getToken()}`,
  },
  baseUrl,
  method: "post",
  params,
  uri: `api/v1/rsvp_send`,
});

const fetchRsvp = async (params: any) => {
  const payload: IConfig = rsvpService(params);

  try {
    const response: any = await callAPIs(payload);
    return response;
  } catch (error) {
    return error;
  }
};

export default fetchRsvp;
