import {callAPIs, IConfig} from "../helpers/api";

const baseUrl = `https://api-ssl.bitly.com`;

const bitlyService = (params: any) => ({
	additionalHeader: {
		"Content-Type": "application/json",
		Authorization: `Bearer 1b076d26892876d69e7c5312d9012279239a23a1`,
	},
	baseUrl,
	method: "post",
	params,
	uri: `v4/shorten`,
});

const fetchBitly = async (params: any) => {
	const payload: IConfig = bitlyService(params);

	try {
		const response: any = await callAPIs(payload);
		return response;
	} catch (error) {
		return error;
	}
};

export default fetchBitly;
