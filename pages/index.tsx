import React from "react";
import { NextPage } from "next";
import HomeContainer from "../containers/home/home";
import { generateToken } from "../helpers/api";
import { fetchHome } from "../services/home";

export interface IResponse {
  access_token: string;
  expires_in: string;
  token_type: string;
  user: { name: string };
}

const HomePage: NextPage = (props: any) => {
  return (
    <HomeContainer auth={props.auth} configuration={props.configuration} />
  );
};

HomePage.getInitialProps = async ({}) => {
  const response: IResponse = await generateToken();
  const homeResponse: any = await fetchHome(response.access_token);

  return { auth: { response }, configuration: homeResponse };
};

export default HomePage;
