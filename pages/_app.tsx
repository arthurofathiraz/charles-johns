import React from "react";
import {AppProps} from "next/app";
import Head from "next/head";
import "../assets/styles.scss";

const App = ({Component, pageProps}: AppProps) => {
	return (
		<>
			<Head>
				<title>Fathariz & Talita - Wedding Invitation</title>
				{/* Primary Meta Tags */}
				<meta name="title" content="Wedding fathariz talita"/>
				<meta name="description" content="Wedding fathariz talita"/>
				<meta name="theme-color" content="#092c52"/>
				{/* Favicon */}
				<link
					rel="icon"
					href="https://manager.fathariztalita.com/storage/public/favicon.ico"
				/>
				{/* Fonts */}
				<link rel="preconnect" href="https://fonts.gstatic.com"/>
				{/* eslint-disable-next-line @next/next/no-page-custom-font */}
				<link
					href="https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,300;0,400;0,700;0,800;1,300;1,400;1,700;1,800&display=swap"
					rel="stylesheet"
				/>
			</Head>
			<Component {...pageProps} />
		</>
	);
};

export default App;
